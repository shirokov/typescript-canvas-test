namespace CanvasTest {
    const TILE_SIZE = 32;
    const TICK_MILLIS = 10;

    class FpsCounter {
        private readonly x: number;
        private readonly y: number;
        private readonly width: number;
        private readonly height: number;

        private frameCount = 0;
        private value = 0;
        private timerMillis = 0;

        constructor(x: number, y: number, width: number, height: number) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        update(millis: number): void {
            this.timerMillis += millis;
            if (this.timerMillis >= 1000) {
                this.value = Math.floor(1000 * this.frameCount / this.timerMillis);

                this.frameCount = 0;
                this.timerMillis = 0;
            }
        }

        draw(context: CanvasRenderingContext2D): void {
            this.frameCount++;

            context.fillStyle = 'white';
            context.fillRect(this.x, this.y, this.width, this.height);
            context.font = '25px Arial';
            context.fillStyle = 'black';
            context.fillText("FPS: " + (this.value > 0 ? this.value : "-"), 10, 30);
        }
    }

    class Environment {
        private readonly canvas: HTMLCanvasElement

        constructor(canvas: HTMLCanvasElement) {
            this.canvas = canvas;
        }

        get width(): number {
            return this.canvas.width;
        }

        get height(): number {
            return this.canvas.height;
        }
    }

    class Thing {
        private readonly width = TILE_SIZE;
        private readonly height = TILE_SIZE;

        private readonly env: Environment;
        private x: number;
        private y: number;

        private static readonly colors: string[] = ['#eb4034', '#eb6e34', '#eb9c34', '#ebcd34', '#e2eb34',
                                                    '#b1eb34', '#89eb34', '#59eb34', '#34eb34', '#34eb5c',
                                                    '#34eb7d', '#34ebae', '#34ebe5', '#34baeb', '#3483eb',
                                                    '#3459eb', '#3471eb', '#4934eb', '#8034eb', '#b134eb',
                                                    '#e234eb', '#eb34c0', '#eb348f', '#eb3465', '#eb3434',];
        private color: number = 0;
        private timerTicks = 0;

        private vX = 2;
        private vY = 2;

        constructor(env: Environment, x: number, y: number) {
            this.env = env;
            this.x = x;
            this.y = y;
        }

        tick(): void {
            this.timerTicks++;
            if (this.timerTicks === 10) {
                this.changeColor();
                this.timerTicks = 0;
            }

            const newX = this.x + this.vX;
            if (newX < 0)
            {
                this.x = 0;
                this.vX *= -1;
            } else if (newX + this.width >= this.env.width)
            {
                this.x = this.env.width - this.width;
                this.vX *= -1;
            }
            else {
                this.x = newX;
            }

            const newY = this.y + this.vY;
            if (newY < 0)
            {
                this.y = 0;
                this.vY *= -1;
            } else if (newY + this.height >= this.env.height)
            {
                this.y = this.env.height - this.height;
                this.vY *= -1;
            }
            else {
                this.y = newY;
            }
        }

        private changeColor() {
            this.color++;
            if (this.color === Thing.colors.length)
            {
                this.color = 0;
            }
        }

        draw(context: CanvasRenderingContext2D): void {
            context.fillStyle = Thing.colors[this.color];
            const x = this.x + this.width / 2;
            const y = this.y + this.height / 2;
            context.beginPath();
            context.arc(x, y, TILE_SIZE / 2, 0, 2 * Math.PI);
            context.fill();
        }
    }

    export class App {
        private canvas: HTMLCanvasElement;
        private context: CanvasRenderingContext2D;
        private lastTimeStamp = 0;
        private tickerTimerMillis = 0;

        private fps: FpsCounter;
        private env: Environment;
        private thing: Thing;

        constructor() {
            this.canvas = <HTMLCanvasElement>document.getElementById("canvas");
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerHeight;
            this.context = App.getContext(this.canvas);

            this.fps = new FpsCounter(0, 0, 110, 40);
            this.env = new Environment(this.canvas);
            this.thing = new Thing(this.env, 100, 100);
        }

        private static getContext(canvas: HTMLCanvasElement): CanvasRenderingContext2D
        {
            let result = canvas.getContext("2d", { alpha: false });
            if (result instanceof CanvasRenderingContext2D) {
                return result;
            }
            throw new Error('failed to obtain context');
        }

        run(): void {
            window.addEventListener('resize', (event) => this.onResize());
            window.requestAnimationFrame((now) => this.draw(now));
        }

        private onResize(): void {
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerHeight;
        }

        private draw(now: number): void {
            const millis = now - this.lastTimeStamp;
            this.doUpdate(millis)
            this.lastTimeStamp = now;

            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.doDraw();
            window.requestAnimationFrame((now) => this.draw(now));
        }

        private doUpdate(millis: number): void {
            this.fps.update(millis);

            this.tickerTimerMillis += millis;
            if (this.tickerTimerMillis >= TICK_MILLIS) {
                this.thing.tick();

                this.tickerTimerMillis = this.tickerTimerMillis % TICK_MILLIS;
            }
        }

        private doDraw(): void
        {
            this.fps.draw(this.context);
            this.thing.draw(this.context);
        }
    }
}

console.log = () => {};

try {
    let app = new CanvasTest.App();
    app.run();
}
catch (e) {
    alert(e);
}